# kep\_web

This project is the sibling project of [wpettersson/kep\_solver](https://gitlab.com/wpettersson/kep_solver) and contains a
web interface ([demo](https://kep-web.optimalmatching.com/)) to a small subset
of the features available in said package.


## Running this yourself

This repository includes [docker](https://www.docker.com) and [docker-compose](https://docs.docker.com/compose/) files, which will need to be installed. Once that is complete, you will want to change `ALLOWED_HOSTS` in `kep_web/settings.py` to contain your site hostname. The site by default will be available on localhost at port 8095 to allow you to add SSL. You can also change this port by editing the `docker-compose.yaml` file.

Once the above changes have been made, you can start the actual server with `docker-compose up --build -d`.
