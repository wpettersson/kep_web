from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse
from django import forms

from json import JSONDecodeError

from kep_solver import fileio, programme, model, graph

SIZE_LIMIT = 100

class SolveInstanceForm(forms.Form):

    max_cycle_length = forms.ChoiceField(
        choices=[(2, "2"), (3, "3"), (4, "4")], initial=3
    )
    max_chain_length = forms.ChoiceField(
        choices=[(1, "1"), (2, "2"), (3, "3")], initial=2
    )
    json = forms.CharField(widget=forms.Textarea,
                           help_text="Input data in JSON or XML formats. See kep_solver documentation for details.")


def index(request):
    return render(request, "solve/index.html")


def uk_index(request):
    if request.method == "POST":
        try:
            instance = fileio.parse_json(request.POST.get("json"))
        except JSONDecodeError as e:
            try:
                instance = fileio.parse_xml(request.POST.get("json"))
            except:
                return render(request, "solve/error.html", {"error": f"JSON parsing failed: {e.msg} at line {e.lineno}, column {e.colno}"  })
        except Exception as e:
            return render(request, "solve/error.html", {"error": "JSON parsing failed: " + e.message})
        if len(instance.recipients) > SIZE_LIMIT:
            return render(
                request,
                "solve/error.html",
                {
                    "error": f"This application supports at most {SIZE_LIMIT} recipients to avoid overloading. Please host your own instance for larger experiments."
                },
            )
        eff = model.EffectiveTwoWay()
        max_size = model.TransplantCount()
        backarcs = model.BackArcs()
        threes = model.ThreeWay()
        score = model.UKScore()
        objectives = [eff, max_size, threes, backarcs, score]
        try:
            max_cycle_length = int(request.POST.get("max_cycle_length"))
            max_chain_length = (
                int(request.POST.get("max_chain_length")) + 1
            )  # NHSBT say a chain of length 1 has two vertices, I say that's a chain of length 2
            this_programme = programme.Programme(
                objectives,
                maxCycleLength=max_cycle_length,
                maxChainLength=max_chain_length,
                description="UKLKSS Objectives",
            )
            solution, _ = this_programme.solve_single(instance)
        except Exception as e:
            return render(
                request, "solve/error.html", {"error": "Instance solving failed" + e.message}
            )
        mygraph = graph.CompatibilityGraph(instance)
        objectives = [[obj, val] for obj, val in zip(objectives, solution.values)]

        def make_short(exchange):
            elts = []
            for vert in exchange:
                if vert.donor.NDD:
                    elts.append(f'<span class="ndd">{vert.donor.id}</span>')
                else:
                    elts.append(f'<span class="recip">{vert.donor.recipient.id}</span>')
            if len(exchange) == 3 and exchange.num_backarcs_uk() >= 1:
                return '<span class="hasbackarc">[' + ", ".join(elts) + "]</span>"
            return "[" + ", ".join(elts) + "]"

        for sol in solution.selected:
            sol.shorttext = make_short(sol.exchange)
        for poss in solution.possible:
            poss.shorttext = make_short(poss.exchange)
        lentwo = len([x for x in solution.possible if len(x.exchange.vertices) == 2])
        lenthree = len([x for x in solution.possible if len(x.exchange.vertices) == 3])
        return render(
            request,
            "solve/solution.html",
            {
                "objectives": objectives,
                "solution": solution,
                "graph": mygraph,
                "lentwo": lentwo,
                "lenthree": lenthree,
            },
        )

    context = {}
    context["form"] = SolveInstanceForm()
    return render(request, "solve/uk/index.html", context)


@csrf_exempt
def uk_json(request):
    if request.method == "POST":
        try:
            instance = fileio.parse_json(request.POST.get("data"))
        except Exception as e:
            return render(request, "solve/error.html", {"error": "JSON parsing failed" + e.message})
        if len(instance.recipients) > SIZE_LIMIT:
            return render(
                request,
                "solve/error.html",
                {
                    "error": "This API supports at most {SIZE_LIMIT} recipients to avoid overloading. Please host your own instance for larger experiments."
                },
            )
        eff = model.EffectiveTwoWay()
        max_size = model.TransplantCount()
        backarcs = model.BackArcs()
        threes = model.ThreeWay()
        score = model.UKScore()
        objectives = [eff, max_size, threes, backarcs, score]
        try:
            max_cycle_length = int(request.POST.get("max_cycle_length", 3))
            max_chain_length = (
                int(request.POST.get("max_chain_length", 2)) + 1
            )  # NHSBT say a chain of length 1 has two vertices, I say that's a chain of length 2
            if max_cycle_length not in [2, 3]:
                return render(
                    request,
                    "solve/error.html",
                    {"error": "max_cycle_length must be one of 2 or 3"},
                )
            if max_chain_length not in [2, 3]:
                print(f"{max_chain_length=}")
                return render(
                    request,
                    "solve/error.html",
                    {"error": "max_chain_length must be one of 1 or 2"},
                )
            this_programme = programme.Programme(
                objectives,
                maxCycleLength=max_cycle_length,
                maxChainLength=max_chain_length,
                description="UKLKSS Objectives",
            )
            solution, this_model = this_programme.solve_single(instance)
        except Exception as e:
            return render(
                request, "solve/error.html", {"error": "Instance solving failed: " + e.message}
            )
        json_obj = fileio.UKJson(this_model, this_programme, solution)
        return HttpResponse(json_obj.to_string(), content_type="application/json")

    return render(request, "solve/uk/index_json.html")
