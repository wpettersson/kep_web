from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("uk/gui", views.uk_index, name="ukgui"),
    path("uk/json", views.uk_json, name="ukjson"),
]
