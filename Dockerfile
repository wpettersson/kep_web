FROM python:3.11

ENV PYTHONBUFFERED 1
ENV PYTHONWRITEBYTECODE 1

RUN useradd --user-group --create-home --no-log-init --shell /bin/bash kepweb

ENV APP_HOME=/home/kepweb/kep_web

RUN mkdir -p $APP_HOME/staticfiles
WORKDIR $APP_HOME

COPY requirements.txt $APP_HOME
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY . $APP_HOME
RUN chown -R kepweb:kepweb $APP_HOME

USER kepweb:kepweb

ENTRYPOINT ["/home/kepweb/kep_web/entrypoint.sh"]

