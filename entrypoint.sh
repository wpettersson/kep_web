#!/bin/sh

# Migrations

python manage.py makemigrations main --noinput
python manage.py migrate --noinput

# Collect static files
python manage.py collectstatic --noinput

exec "$@"
