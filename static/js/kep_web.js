$( document ).ready(function() {
  $('button.load-json').click(function() {
    $.ajax({
      'async': false,
      'url': '/static/jsons/' + this.dataset.file,
      'datatype': 'json',
      'success': function(data) {
      $('textarea#id_json').val(JSON.stringify(data, null, 4));
      }
    });
  });
});
