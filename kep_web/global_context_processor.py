from importlib.metadata import version

def globals(request):
    return {
        "version": version("kep_solver"),
        "testversion": "testing"
    }
