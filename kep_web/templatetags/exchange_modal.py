from django import template

from kep_solver.programme import ModelledExchange
from kep_solver.graph import CompatibilityGraph, Vertex, Exchange

register = template.Library()


@register.inclusion_tag("solve/exchange_modal.html")
def exchange_modal(modelled: ModelledExchange, graph: CompatibilityGraph):
    exchange = modelled.exchange
    cycle = ", ".join(str(v.index) for v in exchange.vertices)

    def to_string_vert(vert: Vertex) -> str:
        if vert.isNdd():
            return str(vert.donor)
        return f"{vert.donor}({vert.donor.recipient})"

    def to_string_exchange(exchange: Exchange) -> str:
        return ", ".join(to_string_vert(v) for v in exchange)

    patdon = to_string_exchange(exchange)
    num_backarcs = 0
    emb_string = ""
    alt_string = ""
    ba_string = ""
    if len(exchange.vertices) == 3:
        num_backarcs = exchange.num_backarcs_uk()
        ba_string = ", ".join(
            f"{exchange.id} [{to_string_exchange(exchange)}]"
            for exchange in exchange.backarc_exchanges_uk()
        )
        alt_string = ", ".join(
            f"{exchange.id} [{to_string_exchange(exchange)}]"
            for exchange in exchange.alternates
        )
        emb_string = ", ".join(
            f"{exchange.id} [{to_string_exchange(exchange)}]"
            for exchange in exchange.embedded
        )
    weight = modelled.values[-1]
    return {
        "ident": exchange.id,
        "cycle": cycle,
        "patdon": patdon,
        "num_backarcs": num_backarcs,
        "weight": weight,
        "ba_string": ba_string,
        "emb_string": emb_string,
        "alt_string": alt_string,
    }
